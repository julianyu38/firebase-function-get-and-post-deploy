import * as functions from 'firebase-functions';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });

const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const firebase = require('firebase');


const app = express();

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS, DELETE');
    res.setHeader('Access-Control-Max-Age', '3600');
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
    next();
});

app.use(cors({ origin: true }));
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ 'extended': 'true' }));
app.use(bodyParser.json());
app.use(bodyParser.json({ type: 'application/vnd.api+json' }));

firebase.initializeApp({
    serviceAccount: "../key/serviceAccount.json",
    databaseURL: "https://fir-function-realtime.firebaseio.com",
    databaseAuthVariableOverride: null
});

const firebase_db = firebase.database();

app.get('/', function (req, res) {
    firebase_db.ref().once('value').then(function (snapshot) {
        const value = snapshot.val() || 'Anoymous';
        const keys = Object.keys(value.informations);
        const lastkey = Number(keys[keys.length - 1]);
        const nextkey = (lastkey + 1).toString();
        const today = new Date();
        const today_date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate() + ' ' + today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
        const firebase_db_ref = firebase_db.ref('informations/' + nextkey);
        
        firebase_db_ref.set(
            {
                UID: nextkey,
                starttime: today_date
            },
            function (err) {
                if (err) {
                    res.send("This is error to save data to db!");
                }
                else {
                    res.send("Data saved on " + nextkey + " successfully.");
                }
            }
        )
    });
});





exports.writedb = functions.https.onRequest(app);

